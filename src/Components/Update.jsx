import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import axios from "axios";
import { add } from "../store/actions/";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";

import "./style.css";

function Update() {
  const [email, setEmail] = useState();
  const [pass, setPass] = useState();
  const [check, setCheck] = useState("false");
  const [id, setID] = useState(null);

  useEffect(() => {
    setID(localStorage.getItem("ID"));
    setEmail(localStorage.getItem("Email"));
    setPass(localStorage.getItem("Password"));
    setCheck(localStorage.getItem("Checkbox Value"));
  }, []);

  //onSubmit
  const onSubmit = (event) => {
    event.preventDefault();
  };

  const updateAPIData = () => {
    axios.put(
      `https://612db70be579e1001791dcac.mockapi.io/submit/users/${id}`,
      {
        email,
        pass,
        check,
      }
    );
  };

  return (
    <div className="update">
      <form onSubmit={onSubmit}>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Email address
          </label>
          <input
            type="email"
            className="form-control"
            aria-describedby="emailHelp"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <div id="emailHelp" className="form-text">
            We'll never share your email with anyone else.
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            value={pass}
            onChange={(event) => {
              setPass(event.target.value);
            }}
          />
        </div>
        <div className="mb-3 form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
            onClick={(e) => {
              setCheck("true");
            }}
          />
          <label className="form-check-label" htmlFor="exampleCheck1">
            I agree to the Terms and Conditions
          </label>
        </div>
        <Link to="/read" exact>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={updateAPIData}
          >
            Update
          </button>
        </Link>
      </form>
    </div>
  );
}

export default Update;
