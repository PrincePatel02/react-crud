import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import axios from "axios";
import { add } from "../store/actions/";
import app from "../utils/firebase";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./style.css";

function Create() {
  const [email, setEmail] = useState();
  const [pass, setPass] = useState();
  const [check, setCheck] = useState("false");
  const dispatch = useDispatch();

  //onSubmit
  const onSubmit = (event) => {
    event.preventDefault();
    if (email !== "" && pass !== "" && check !== "") {
      console.log(email, pass, check);
      dispatch(add(email, pass, check));
      axios.post("https://612db70be579e1001791dcac.mockapi.io/submit/users", {
        email,
        pass,
        check,
      });

      setEmail("");
      setPass("");
      setCheck("false");
    } else {
      alert("Filed is empty.");
    }
  };

  const onSubmitHandler = () => {
    
  };

  return (
    <div className="create">
      <form onSubmit={onSubmit}>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Email address
          </label>
          <input
            type="email"
            className="form-control"
            aria-describedby="emailHelp"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <div id="emailHelp" className="form-text">
            We'll never share your email with anyone else.
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            value={pass}
            onChange={(event) => {
              setPass(event.target.value);
            }}
          />
        </div>
        <div className="mb-3 form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
            onClick={(e) => {
              setCheck("true");
            }}
          />
          <label className="form-check-label" htmlFor="exampleCheck1">
            Check me out
          </label>
        </div>
        <Link to="/read" exact>
          <button
            type="submit"
            className="btn btn-primary"
            onSubmit={onSubmitHandler}
          >
            Submit
          </button>
        </Link>
      </form>
    </div>
  );
}

export default Create;
