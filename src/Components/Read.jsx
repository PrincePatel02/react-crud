import React, { useEffect, useState } from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useHistory, Link } from "react-router-dom";
import axios from "axios";
function Read() {
  let history = useHistory();

  useEffect(() => {
    axios
      .get("https://612db70be579e1001791dcac.mockapi.io/submit/users")
      .then((response) => {
        setAPIData(response.data);
      });

    console.log(APIData);
  }, []);
  const [APIData, setAPIData] = useState([]);

  const setData = (data) => {
    console.log(data);
    let { id, email, pass, check } = data;
    localStorage.setItem("ID", id);
    localStorage.setItem("Email", email);
    localStorage.setItem("Password", pass);
    localStorage.setItem("Checkbox Value", check);
  };

  const onDelete = (val) => {
    axios.delete(
      `https://612db70be579e1001791dcac.mockapi.io/submit/users/${val.id}`
    );
  };

  return (
    <div>
      <button
        className="btn btn-primary"
        onClick={() => {
          history.push("/");
        }}
      >
        Back
      </button>
      <table class="table table-dark">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Email</th>
            <th scope="col">Pass</th>
            <th scope="col">Check</th>
            <th scope="col">Update</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          {APIData.map((val, index) => {
            return (
              <>
                <tr key={index}>
                  <th>{index + 1}</th>
                  <td>{val.email}</td>
                  <td>{val.pass}</td>
                  <td>{val.check}</td>
                  <td>
                    <Link to="/update">
                      <button
                        className="btn btn-info"
                        onClick={() => {
                          setData(val);
                        }}
                      >
                        Update
                      </button>
                    </Link>
                  </td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        onDelete(val);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              </>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Read;
