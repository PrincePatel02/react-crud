export const add = (email, pass, check) => {
  return {
    type: "ADD",
    payload: {
      email: email,
      pass: pass,
      check: check,
    },
  };
};
