import { combineReducers } from "redux";
import formReducer from "./formReducer";
const rootReducer = combineReducers({
  Form_Date: formReducer,
});

export default rootReducer;
