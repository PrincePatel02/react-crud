import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Read from "./Components/Read";
import Create from "./Components/Create";
import Update from "./Components/Update";
import "./App.css";

function App() {
  return (
    <Router>
      <Switch>
        <div className="App">
          <h2 className="main-header">React Crud Operations</h2>
          <div>
            <Route path="/" exact>
              <Create />
            </Route>
            <Route path="/read">
              <Read />
            </Route>
            <Route path="/update">
              <Update />
            </Route>
          </div>
        </div>
      </Switch>
    </Router>
  );
}

export default App;
